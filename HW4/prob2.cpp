/* Main routine to test the Adams-Bashforth-3 method on the scalar-valued ODE problem 
     y' = -exp(-t)*y, t in [0,1],
     y(0) = 1.

   Lu Zhang
   Math 6321 @ SMU
   Fall 2016  */

#include <iostream>
#include <vector>
#include "matrix.hpp"
#include "rhs.hpp"
#include "erk4.hpp"
#include "ab3.hpp"

using namespace std;

// ODE RHS function
class MyRHS: public RHSFunction {
public:
  int Evaluate(double t, vector<double>& y, vector<double>& f) {
    f[0] = -exp(-t)*y[0];
    return 0;
  }
};

// Convenience function for analytical solution
vector<double> ytrue(const double t) { 
  vector<double> yt = {exp(exp(-t)-1.0)};
  return yt;
};

// main routine
int main() {

  // time steps to try
  vector<double> h = {0.1, 0.05, 0.01, 0.005, 0.001, 0.0005};

  // set problem information
  vector<double> y0 = {1.0};
  double t0 = 0.0;
  double Tf = 5.0;
  double dtout = 1.0;

  // create ODE RHS function objects
  MyRHS rhs;
 
  // create AB3 and RK4 stepper objects
  ERK4Stepper ERK4(rhs, y0);
  AB3Stepper AB3(rhs, y0);

  
  // storage for errors
  double *abserrs;
  abserrs = new double[h.size()];

  // loop over time step sizes
  for (int ih=0; ih<h.size(); ih++) {
    cout << endl;
    cout << "when h = " << h[ih] << endl;

    // set the initial conditions by ERK4
    vector<double> ytem(y0); 
    vector<double> y1tem(y0);
    vector<double> y2tem(y0);
    vector<double> y3tem(y0);
    vector<double> tspan1;
    tspan1 = {0, h[ih]};
    ERK4.Evolve(tspan1, h[ih], y1tem);
    vector<double> tspan2;
    tspan2 = {0, 2.0*h[ih]};
    ERK4.Evolve(tspan2, h[ih], y2tem);
    vector<double> tspan3;
    tspan3 = {0, 3.0*h[ih]};
    ERK4.Evolve(tspan3, h[ih], y3tem);
    vector<double> y3(ytem);
    vector<double> y2(y1tem);
    vector<double> y1(y2tem);
    vector<double> y(y3tem);
    

    // set initial time
    double tcur = t0;

    // reset maxerr
    double maxerr = 0.0;
    
    // set a counter for the interval(the first interval is a special one)
    int num = 0;
     
    // loop over output step sizes: call solver and output error
    while (tcur < 0.99999*Tf) {

      vector<double> tspan;
      num = num + 1;
      
      if(num == 1){
      tcur = tcur + 3*h[ih];
        // set the time interval for this solve
        tspan = {tcur, std::min(tcur-3*h[ih] + dtout, Tf)};
      }
      else{
	// set the time interval for this solve
        tspan = {tcur, std::min(tcur + dtout, Tf)};
      }

      // call the solver, update current time
      vector<double> tvals = AB3.Evolve(tspan, h[ih], y, y1, y2, y3);
      tcur = tvals.back();     // last entry in tvals

    	// compute the error at tcur, output to screen and accumulate maximum
	vector<double> yerr = y - ytrue(tcur);
	double err = InfNorm(yerr);
	maxerr = std::max(maxerr, err);
	
	cout << "  y(" << tcur << ") = " << y[0]
	     << "  \t||error|| = " << err
	     << endl;
      }
      abserrs[ih] = maxerr;
      cout << "Max error = " << maxerr << endl;
    }

    // calculate orders of convergence between successive values of h (absolute value)
    cout << "\nConvergence order estimate:\n";
    for(int ih=0; ih<h.size()-1;ih++){
      double dlogh = log(h[ih+1]) - log(h[ih]);
      double dloge = log(abserrs[ih+1]) - log(abserrs[ih]);
      cout << " order = " << dloge/dlogh << endl;
    }
    delete[] abserrs;
    return 0;
}
