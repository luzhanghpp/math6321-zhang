/* Adaptive Runge-Kutta-Fehlberg stepper class header file.

   Lu ZHang
   Math 6321 @ SMU
   Fall 2016  */

#ifndef ADAPT_RUNGE_KUTTA_FEHLBERG_DEFINED__
#define ADAPT_RUNGE_KUTTA_FEHLBERG_DEFINED__

// Inclusions
#include <vector>
#include <math.h>
#include "matrix.hpp"
#include "rhs.hpp"


// Adaptive Runge-Kutta-Fehlberg time stepper class
class AdaptRKF {

 private:

  // private reusable local data
  RHSFunction *frhs;          // pointer to ODE RHS function
  std::vector<double> z, fn, f0, f1, f2, f3, f4, f5 ;     // local vector storage
  std::vector<double> yerr;
  Matrix A;
  std::vector<double> b, btilta, c;

 public:

  double rtol;        // desired relative solution error
  double atol;        // desired absolute solution error
  double grow;        // maximum step size growth factor
  double safe;        // safety factor for step size estimate
  double fail;        // failed step reduction factor
  double ONEMSM;      // safety factors for
  double ONEPSM;      // floating-point comparisons
  double alpha;       // exponent relating step to error
  double error_norm;  // current estimate of the local error ratio
  double h;           // current time step size
  long int fails;     // number of failed steps
  long int steps;     // number of successful steps
  long int maxit;     // maximum number of steps

  // constructor (sets RHS function pointer & solver parameters, copies y for local data)
  AdaptRKF(RHSFunction& frhs_, double rtol_, double atol_, std::vector<double>& y);

  // Evolve routine (evolves the solution via adaptive RKF)
  std::vector<double> Evolve(std::vector<double>& tspan, std::vector<double>& y);

  // Single step calculation
  int Step(double t, double h, std::vector<double>& y, std::vector<double> & ytilta);

};

#endif
