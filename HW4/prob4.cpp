/* problem 4: test adaptive Runge-Kutta-Fehlberg for the 
   scalar-valued ODE problem 
     y' = -y + 2*cos(t), t in [0,1],
     y(0) = 1.

   Lu Zhang
   Math 6321 @ SMU
   Fall 2016  */

#include <iostream>
#include <iomanip>
#include <vector>
#include "matrix.hpp"
#include "rhs.hpp"
#include "adapt_rkf.hpp"
#include "adapt_euler.hpp"

using namespace std;


// ODE RHS function class -- instantiates a RHSFunction
class MyRHS: public RHSFunction {
public:
  int Evaluate(double t, vector<double>& y, vector<double>& f) {
    f[0] = -y[0] + 2*cos(t);
    return 0;
  }
};
//    Convenience function for analytical solution
vector<double> ytrue(const double t) { 
  vector<double> yt(1);
  yt[0] = cos(t) + sin(t);
  return yt;
};

// main routine
int main() {

  // tolerances to try
  vector<double> rtols = {1.e-4, 1.e-6, 1.e-8};
  vector<double> atols = {1.e-6, 1.e-8, 1.e-10};

  // initial condition and time span
  vector<double> y0 = {1.0};
  double t0 = 0.0;
  double Tf = 10.0;
  double tcur = t0;
  double dtout = 1.0;

  // create ODE RHS function objects
  MyRHS rhs;

  // create Adaptive Runge-Kutta-Fehlberg and Adaptive Forward Euler stepper object (will reset rtol before each solve)
   AdaptRKF ARKF(rhs, 0.0, 0.0, y0);
   AdaptEuler AE(rhs, 0.0, 0.0, y0);
   
   //////////Adaptive RKF////////
   cout << "\nRunning with adaptive RKF:" << endl;
   
  // loop over relative tolerances
  for (int ir=0; ir<rtols.size(); ir++) {
 
    // set up the problem for this tolerance
    ARKF.rtol = rtols[ir];
    ARKF.atol = atols[ir];
    
    vector<double> y = y0;
    tcur = t0;
    double maxabserr = 0.0;
    double maxrelerr = 0.0;
    long int totsteps = 0;
    long int totfails = 0;

    //   loop over output step sizes: call solver and output error
    while (tcur < 0.99999*Tf) {
      
      // set the time interval for this solve
      vector<double> tspan = {tcur, std::min(tcur + dtout, Tf)};

      // call the solver for this time interval
      vector<double> tvals = ARKF.Evolve(tspan, y);
      tcur = tvals.back();  // last entry in tvals
      totsteps += ARKF.steps;
      totfails += ARKF.fails;

      // compute the errors at tcur, and accumulate maxabserr, maxrelerr
      vector<double> yerr = y - ytrue(tcur);
      double abserr = InfNorm(yerr);
      double relerr = abserr / InfNorm(ytrue(tcur));
      maxabserr = std::max(maxabserr, abserr);
      maxrelerr = std::max(maxrelerr, relerr);  
    }

    // output final results for this tolerance
    cout << "\nOverall results for rtol = " << ARKF.rtol << ",  atol = " << ARKF.atol << ":\n"
	 << "   maxrelerr = " << maxrelerr << endl
	 << "   steps = " << totsteps << endl
	 << "   fails = " << totfails << endl;

  }

  ///////////////Adaptive Forward Euler////////////
  cout << "\nRunning with adaptive forward euler:" << endl;
  
  // loop over relative tolerances
  for (int ir=0; ir<rtols.size(); ir++) {
 
    // set up the problem for this tolerance
    AE.rtol = rtols[ir];
    AE.atol = atols[ir];
    vector<double> y = y0;
    tcur = t0;
    double maxabserr = 0.0;
    double maxrelerr = 0.0;
    long int totsteps = 0;
    long int totfails = 0;

    //   loop over output step sizes: call solver and output error
    while (tcur < 0.99999*Tf) {
      
      // set the time interval for this solve
      vector<double> tspan = {tcur, std::min(tcur + dtout, Tf)};

      // call the solver for this time interval
      vector<double> tvals = AE.Evolve(tspan, y);
      tcur = tvals.back();  // last entry in tvals
      totsteps += AE.steps;
      totfails += AE.fails;

      // compute the errors at tcur, accumulate maxima
      vector<double> yerr = y - ytrue(tcur);
       double abserr = InfNorm(yerr);
      double relerr = abserr / InfNorm(ytrue(tcur));
      maxabserr = std::max(maxabserr, abserr);
      maxrelerr = std::max(maxrelerr, relerr);  
    }

    // output final results for this tolerance
    cout << "\nOverall results for rtol = " << AE.rtol <<",   atol = " << AE.atol<< ":\n"
	 << "   maxrelerr = " << maxrelerr << endl
	 << "   steps = " << totsteps << endl
	 << "   fails = " << totfails << endl;

  }

  return 0;
}



