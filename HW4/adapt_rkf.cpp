/* Adaptive Runge-Kutta-Fehlberg solver class implementation file.

   Class to perform time evolution of the IVP
        y' = f(t,y),  t in [t0, Tf],  y(t0) = y0
   using the Runge-Kutta-Fehlberg time stepping method. 

   LU Zhang
   Math 6321 @ SMU
   Fall 2016  */

#include "matrix.hpp"
#include "adapt_rkf.hpp"



// Adaptive Runge-Kutta-Fehlberg class constructor routine
//
// Inputs:  frhs_ holds the ODE RHSFunction object, f(t,y)
//          rtol holds the desired relative solution accuracy
//          atol holds the desired absolute solution accuracy
// 
// Sets default values for adaptivity parameters, all of which may
// be modified by the user after the solver object has been created
AdaptRKF::AdaptRKF(RHSFunction& frhs_, double rtol_, 
                       double atol_, std::vector<double>& y) {
  frhs = &frhs_;    // set RHSFunction pointer
  rtol = rtol_;     // set tolerances
  atol = atol_;
  z = y;            // clone y to create local vectors
  f0 = y;
  f1 = y;
  f2 = y;
  f3 = y;
  f4 = y;
  f5 = y;
  fn = y;
  yerr = y;

  A = Matrix(6,6);                   // Butcher table data
  A(1,0) = 1.0/4.0;
  A(2,0) = 3.0/32.0;
  A(3,0) = 1932.0/2197.0;
  A(4,0) = 439.0/216.0;
  A(5,0) = -8.0/27.0;
  A(2,1) = 9.0/32.0;
  A(3,1) = -7200.0/2197.0;
  A(4,1) = -8.0;
  A(5,1) = 2.0;
  A(3,2) = 7296.0/2197.0;
  A(4,2) = 3680.0/513.0;
  A(5,2) = -3544.0/2565.0;
  A(4,3) = -845.0/4104.0;
  A(5,3) = 1859.0/4104.0;
  A(5,4) = -11.0/40.0;
  b = {16.0/135.0, 0.0, 6656.0/12825.0, 28561.0/56430.0, -9.0/50.0, 2.0/55.0};
  btilta = {25.0/216.0, 0.0, 1408.0/2565.0, 2197.0/4104.0, -1.0/5.0, 0.0};
  c = {0.0, 1.0/4.0, 3.0/8.0, 12.0/13.0, 1.0, 1.0/2.0};
  

  maxit = 1e6;      // set default solver parameters
  grow = 50.0;
  safe = 0.95;
  fail = 0.5;
  ONEMSM = 1.0 - 1.e-8;
  ONEPSM = 1.0 + 1.e-8;
  alpha = -0.5;
  fails = 0;
  steps = 0;
  error_norm = 0.0;
  h = 0.0;
};


// The adaptive Runge-Kutta-Fehlberg time step evolution routine
//
// Inputs:  tspan holds the current time interval, [t0, tf]
//          y holds the initial condition, y(t0)
// Outputs: y holds the computed solution, y(tf)
//
// The return value is a row vector containing all internal 
// times at which the solution was computed,
//               [t0, t1, ..., tN]
std::vector<double> AdaptRKF::Evolve(std::vector<double>& tspan, 
                                       std::vector<double>& y) {

  // initialize output
  std::vector<double> tvals = {tspan[0]};

  // check for positive tolerances
  if ((rtol <= 0.0) || (atol <= 0.0)) {
    std::cerr << "Evolve error: illegal tolerances, atol = " 
	      << atol << ",  rtol = " << rtol << std::endl;
    return tvals;
  }

  // reset solver statistics
  fails = 0;
  steps = 0;

  // get |y'(t0)|
  if (frhs->Evaluate(tspan[0], y, fn) != 0) {
    std::cerr << "Evolve error in RHS function\n";
    return tvals;
  }

  // estimate initial h value via linearization, safety factor
  error_norm = std::max(Norm(fn) / ( rtol * Norm(y) + atol ), 1.e-8);
  h = safe/error_norm;
  if (tspan[0]+h > tspan[1])   h = tspan[1]-tspan[0];

  // iterate over time steps (all but the last one)
  for (int tstep=1; tstep<=maxit; tstep++) {

    std::vector<double>  y2(y);
    std::vector<double>  y1(y);
    // get RHS at this time by the order 5 Runge Kutta
    if (Step(tvals[steps], h, y1, y2) != 0) {
      std::cerr << "Evolve error in RHS function\n";
      return tvals;
    }

    // compute error estimate
    yerr = y1 - y2;

    // compute error estimate success factor
    error_norm = std::max(InfNorm(yerr) / ( rtol * InfNorm(y2) + atol ), 1.e-8);

    // if solution has too much error: reduce step size, increment failure counter, and retry
    if (error_norm > ONEPSM) {
      h *= fail;
      fails++;
      continue;
    }

    // successful step
    tvals.push_back(tvals[steps++] + h);  // append updated time, increment step counter
    y = y1;

    // exit loop if we've reached the final time
    if ( tvals[steps] >= tspan[1]*ONEMSM )  break;

    // pick next time step size based on this error estimate
    double eta = safe*std::pow(error_norm, alpha);    // step size estimate
    eta = std::min(eta, grow);                        // maximum growth
    h *= eta;                                         // update h
    h = std::min(h, tspan[1]-tvals[steps]);           // don't pass Tf
  }

  // set output array as the subset of tvals that we actually used
  return tvals;
}


// Single step of Runge-Kutta-Fehlberg method
//
// Inputs:  t holds the current time
//          h holds the current time step size
//          z, f0-f5 hold temporary vectors needed for the problem
//          y holds the current solution
// Outputs: y holds the updated solution, y(t+h)
//
// The return value is an integer indicating success/failure,
// with 0 indicating success, and nonzero failure.
int AdaptRKF::Step(double t, double h, std::vector<double>& y, std::vector<double> & ytilta) {

  // stage 1: set stage and compute RHS
  z = y;
  if (frhs->Evaluate(t, z, f0) != 0) {
    std::cerr << "Step: Error in ODE RHS function\n";
    return 1;
  }

  // stage 2: set stage and compute RHS
  z = y + (h*A(1,0))*f0;
  if (frhs->Evaluate(t+c[1]*h, z, f1) != 0) {
    std::cerr << "Step: Error in ODE RHS function\n";
    return 1;
  }

  // stage 3: set stage and compute RHS
  z = y + h*(A(2,0)*f0 + A(2,1)*f1);
  if (frhs->Evaluate(t+c[2]*h, z, f2) != 0) {
    std::cerr << "Step: Error in ODE RHS function\n";
    return 1;
  }

  // stage 4: set stage and compute RHS
  z = y + h*(A(3,0)*f0 + A(3,1)*f1 + A(3,2)*f2);
  if (frhs->Evaluate(t+c[3]*h, z, f3) != 0) {
    std::cerr << "Step: Error in ODE RHS function\n";
    return 1;
  }

  // stage 5: set stage and compute RHS
  z = y + h*(A(4,0)*f0 + A(4,1)*f1 + A(4,2)*f2 + A(4,3)*f3);
  if (frhs->Evaluate(t+c[4]*h, z, f4) != 0) {
    std::cerr << "Step: Error in ODE RHS function\n";
    return 1;
  }

  // stage 6: set stage and compute RHS
  z = y + h*(A(5,0)*f0 + A(5,1)*f1 + A(5,2)*f2 + A(5,3)*f3 + A(5,4)*f4);
  if (frhs->Evaluate(t+c[5]*h, z, f5) != 0) {
    std::cerr << "Step: Error in ODE RHS function\n";
    return 1;
  }

  // compute next step solution by 4th order Runge kutta method
  ytilta = y + h*(btilta[0]*f0 + btilta[1]*f1 + btilta[2]*f2 + btilta[3]*f3 + btilta[4]*f4 + btilta[5]*f5);
  
  // compute next step solution by 5th order Runge kutta method
  y += h*(b[0]*f0 + b[1]*f1 + b[2]*f2 + b[3]*f3 + b[4]*f4 + b[5]*f5);

  // return success
  return 0;
}
