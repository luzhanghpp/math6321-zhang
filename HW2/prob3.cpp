
/* Main routine to test the Adaptive Euler method for some scalar-valued ODE problems
     y' = f(t,y), t in [0,5],
     y(0) = y0.

   Lu Zhang
   Math 6321 @ SMU
   Fall 2016  */

#include <iostream>
#include <vector>
#include "matrix.hpp"
#include "rhs.hpp"
#include "adapt_euler.hpp"

using namespace std;


// Problem :  y' = -e^(-t)*y
//    ODE RHS function class -- instantiates a RHSFunction
class RHS: public RHSFunction {
public:
  int Evaluate(double t, vector<double>& y, vector<double>& f) {    // evaluates the RHS function, f(t,y)
    f = -exp(-1.0*t)*y;
    return 0;
  }
};
// Convenience function for analytical solution
vector<double> ytrue(const double t) { 
  vector<double> yt(1);
  yt[0] = exp(exp(-t)-1.0);
  return yt;
};


// main routine
int main() {

  // relative tolerance and absolute tolerance to try
  vector<double> rtol = {1e-2,1e-4,1e-6,1e-8};

  // set our absolute tolerance
  double atol = 1e-11;

  // choose a time step size
  double h1 = 1;

  // set problem information
  vector<double> y0 = {1.0};
  double t0 = 0.0;
  double Tf = 5.0;
  double tcur = t0;
  int dtout = 1;

  // create ODE RHS function objects
  RHS f;

  // loop over time step sizes
  for (int ir=0; ir<rtol.size(); ir++) {

    // create adaptive euler object
     AdaptEuler AE(f,rtol[ir],atol,y0);

    // initialize our time step size h at Evolve function
     AE.h = h1;

    // initialize y
    vector<double> y = y0;
    tcur = t0; // give value to tcur

    // maxaerr is the maximum absolute error
    // maxrerr is the maximum relative error
    // totalsteps is the total steps used to get the value of y at the final time
    double maxaerr = 0.0;
    double maxrerr = 0.0;
    int totalsteps = 0;
    
    cout << "\nRunning problem with relative tolerance rtol = " << rtol[ir] << ":\n";

    //   loop over output step sizes: call solver and output error
    while (tcur < 0.99999*Tf) {
      
      // set the time interval for this solve
      vector<double> tspan = {tcur, std::min(tcur + dtout, Tf)};

      // call the solver, update current time
      vector<double> tvals = AE.Evolve(tspan,y);
      int tvalsize = tvals.size(); // the size of tvals
      tcur = tvals.at(tvalsize-2);  // penultimate entry in tvals
      totalsteps = totalsteps + tvals.back(); // the last entry of tvals is the totalsteps of one tspan

      // compute the absolute error which is aerr at tcur
      vector<double> yaerr = y - ytrue(tcur);
      double aerr = InfNorm(yaerr);
      
      // compute the relative error which is yrerr at tcur
      double rerr = InfNorm(y - ytrue(tcur))/InfNorm(ytrue(tcur)); 

      // get the maximum absolute error which is maxaerr
      maxaerr = std::max(maxaerr, aerr);

      // get the maximum relative error which is maxrerr
      maxrerr = std::max(maxrerr, rerr);
      

      // output to screen
      cout << "  y(" << tcur << ") = " << y[0]
	   << "  \t absolte_error = " << aerr
	   << "  \t relative_error = " << rerr
	   << endl;
    }
    cout << "Max absolute error = " << maxaerr << endl;
    cout << "Max relative error = " << maxrerr << endl;
    cout << endl;
    cout << "the total steps for this problem is: "<<totalsteps<<endl;
  }

  return 0;
}
