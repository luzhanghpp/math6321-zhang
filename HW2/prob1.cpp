/* Main routine to test the forward Euler method for some scalar-valued ODE problems
     y' = f(t,y), t in [0,5],
     y(0) = y0.

   Lu Zhang
   Math 6321 @ SMU
   Fall 2016  */

#include <iostream>
#include <vector>
#include "matrix.hpp"
#include "rhs.hpp"
#include "fwd_euler.hpp"

using namespace std;


// Problem :  y' = -e^(-t)*y
//    ODE RHS function class -- instantiates a RHSFunction
class RHS1: public RHSFunction {
public:
  int Evaluate(double t, vector<double>& y, vector<double>& f) {    // evaluates the RHS function, f(t,y)
    f = -exp(-1.0*t)*y;
    return 0;
  }
};
//    Convenience function for analytical solution
vector<double> ytrue(const double t) { 
  vector<double> yt(1);
  yt[0] = exp(exp(-t)-1.0);
  return yt;
};


// main routine
int main() {

  // time steps to try
  vector<double> h = {0.2, 0.1, 0.05, 0.025, 0.0125};

  // set problem information
  vector<double> y0 = {1.0};
  double t0 = 0.0;
  double Tf = 5.0;
  double tcur = t0;
  double dtout = 1.0;

  // create ODE RHS function objects
  RHS1 f1;

  // create forward Euler stepper object
  ForwardEulerStepper FE(f1, y0);

  // creat a vector to collect the error of every h
  Matrix errors(5,2);
  
  // give h values to the first column of errors
  errors(0,0) = 0.2;
  errors(1,0) = 0.1;
  errors(2,0) = 0.05;
  errors(3,0) = 0.025;
  errors(4,0) = 0.0125;

  // loop over time step sizes
  for (int ih=0; ih<h.size(); ih++) {

    // problem :
    vector<double> y = y0;
    tcur = t0;
    double maxerr = 0.0;
    cout << "\nRunning problem with stepsize h = " << h[ih] << ":\n";

    //   loop over output step sizes: call solver and output error
    while (tcur < 0.99999*Tf) {
      
      // set the time interval for this solve
      vector<double> tspan = {tcur, std::min(tcur + dtout, Tf)};

      // call the solver, update current time
      vector<double> tvals = FE.Evolve(tspan, h[ih], y);
      tcur = tvals.back();   // last entry in tvals

      // compute the error at tcur, output to screen and accumulate maximum
      vector<double> yerr = y - ytrue(tcur);
      double err = InfNorm(yerr);
      maxerr = std::max(maxerr, err);
      cout << "  y(" << tcur << ") = " << y[0]
	   << "  \t||error|| = " << err
	   << endl;
    }
    cout << "Max error = " << maxerr << endl;
    errors(ih,1) = maxerr;
  }
  cout << endl;
  cout << "Max error of each h (the first column is for h and the second column is for maximum error):" << endl;
  cout << errors << endl;
  cout << "According to the relationship of h and error above, the error is reduced by half almostly when h is reduced by half, then we can conclude:"<<endl;
  cout << "the order of convergence for Euler's method on this problem is O(h)."<<endl;
  cout << endl;
  return 0;
}
