/* Adaptive Euler slover class header file.

   Lu Zhang
   Math 6321 @ SMU
   Fall 2016  */

#ifndef ADAPTIVE_EULER_DEFINED__
#define ADAPTIVE_EULER_DEFINED__

// Inclusions
#include <vector>
#include <math.h>
#include "matrix.hpp"
#include "rhs.hpp"


// Adaptive Euler solver class
class AdaptEuler {

 private:

  // private reusable local data
  std::vector<double> f;   // storage for ODE RHS vector
  //std::vector<double> ftilta;
  //std::vector<double> ytilta;
  RHSFunction *frhs;       // pointer to ODE RHS function

 public:

  // public parameters
  double h;  // suggestion for the first step size
  double rtol_; // relative tolerance
  double atol_; // absolute tolerance
  std::vector<double> ftilta;
  std::vector<double> ytilta;

  // constructor (sets RHS function pointer, copies y for local data)
  AdaptEuler(RHSFunction& frhs_, double rtol, double atol, std::vector<double>& y) {
    frhs = &frhs_;
    f = y;
    ftilta = y;
    ytilta = y;
    rtol_ = rtol;
    atol_ = atol;
  };

  // Evolve routine (evolves the solution via Adaptive Euler)
  std::vector<double> Evolve(std::vector<double>& tspan, std::vector<double>& y);

};

#endif
