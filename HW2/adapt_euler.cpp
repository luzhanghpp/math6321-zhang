/* Adaptive Euler solver class implementation file.

   Class to perform time evolution of the IVP
        y' = f(t,y),  t in [t0, Tf],  y(t0) = y0
   using the adaptive forward Euler (explicit Euler) method. 

   Lu Zhang
   Math 6321 @ SMU
   Fall 2016 */

#include <vector>
#include "matrix.hpp"
#include "adapt_euler.hpp"


// The adaptive Euler routine
//
// Inputs:  tspan holds the current time interval, [t0, tf]
//          y holds the initial condition, y(t0)
// Outputs: y holds the computed solution, y(tf)
//
// The return value is a row vector containing all internal 
// times at which the solution was computed,
//               [t0, t1, ..., tN]
std::vector<double> AdaptEuler::Evolve(std::vector<double>& tspan, std::vector<double>& y) {

  // initialize output vector
  std::vector<double> times = {tspan[0]};

  // check for legal inputs 
  if (tspan[1] <= tspan[0]) {
    std::cerr << "AdaptEuler: Illegal tspan\n";
    return times;	  
  }

  // initialize the number of attempting h of loop i
  int timestep1 = 0;
  
  // interate over time step
  for(int i = 0; i<1e6; i++){

    // check if we get to tf
     if(times[i]==tspan[1]){
      break;
    }

    // make sure to get the last time at tf exactly
    if(times[i]+h>tspan[1]){
      h = tspan[1] - times[i];
    }
    
    // y1 is the copy of y; ytilta1 is also the copy of y
    std::vector<double>  y1 = y; // initialize y1
    std::vector<double>  ytilta1 = y; // initialize ytilta1

    // initialize the time steps of loop k
    for(int k=0;k<1e6;k++){
      
      y = y1; // make sure we have same initial value 
      ytilta = ytilta1; // make sure we have same initial value
      
      // compute ODE RHS
      if (frhs->Evaluate(times[i], y, f) != 0) {
	std::cerr << "AdaptEuler: Error in ODE RHS function\n";
	return times;
      }
    
      // update solution with forward Euler step when step size is h
      y += (h*f); // this compute solution is at times[i+1]

      // set another time step which is a half of step size h
      double htilta = h/2;

      // use the new time step which is h/2 to get the compute solution at times[i+1]
      for(int j=0;j<2;j++){
	if(frhs->Evaluate(times[i]+j*h/2,ytilta,ftilta) != 0){
	  std::cerr << "AdaptEuler: Error in ODE RHS function\n";
	  return times;
	}
	ytilta += (htilta*ftilta);
      }

      // rules of updating the time step size h
      double end_condition = InfNorm(2*ytilta-2*y)-(rtol_*InfNorm(2*ytilta-y)+atol_);
      if(end_condition<0){
        times.push_back(times[i]+h);
	h = 1.5*h;
	break;
      }
      else if(end_condition==0){
	times.push_back(times[i]+h);
       	h = h;
	break;
      }
      else if(end_condition>0){
	h = h/2;
        timestep1 += 1;
      }
    }
    y = 2*ytilta - y; // update our y by Richardson extrapolant
  }
  times.push_back(timestep1); // set our total step of i loop to the last entry of times
  return times;
}
    

    
