/* problem 2: use adaptive Forward Euler to solve the 
   system ODE problem 
     y1' = -3y1 + y2 -e^(-2t), t in [0,3],
     y2' = y1 - 3y2 + e^(-t), t in [0,3],
     y1(0) = 2,
     y2(0) = 1.

   Lu Zhang
   Math 6321 @ SMU
   Fall 2016  */

#include <iostream>
#include <iomanip>
#include <vector>
#include "matrix.hpp"
#include "rhs.hpp"
#include "adapt_euler.hpp"

using namespace std;


// ODE RHS function class -- instantiates a RHSFunction
class MyRHS: public RHSFunction {
public:
  int Evaluate(double t, vector<double>& y, vector<double>& f) {
    f[0] = -3.0*y[0] + y[1] -exp(-2.0*t);
    f[1] = y[0] -3*y[1] +exp(-t);
    return 0;
  }
};
//    Convenience function for analytical solution
vector<double> ytrue(const double t) { 
  vector<double> yt(2);
  yt[0] = 1.0/3.0*exp(-t) + (3.0/4.0-1.0/2.0*t)*exp(-2.0*t) + 11.0/12.0*exp(-4.0*t);
  yt[1] = 2.0/3.0*exp(-t) + (5.0/4.0-1.0/2.0*t)*exp(-2.0*t) - 11.0/12.0*exp(-4.0*t);
  return yt;
};


// main routine
int main() {

  // tolerances to try
  vector<double> rtols = {1.e-2, 1.e-4, 1.e-6, 1.e-8};
  double atol = 1.e-12;

  // initial condition and time span
  vector<double> y0 = {2.0,1.0};
  double t0 = 0.0;
  double Tf = 3.0;
  double tcur = t0;
  double dtout = 0.3;

  // create ODE RHS function objects
  MyRHS rhs;

  // create forward Euler stepper object (will reset rtol before each solve)
  AdaptEuler AE(rhs, 0.0, atol, y0);

  // loop over relative tolerances
  for (int ir=0; ir<rtols.size(); ir++) {
 
    // set up the problem for this tolerance
    AE.rtol = rtols[ir];
    vector<double> y = y0;
    tcur = t0;
    double maxabserr = 0.0;
    double maxerr = 0.0;
    long int totsteps = 0;
    long int totfails = 0;
    cout << "\nRunning problem 2 with rtol = " << AE.rtol
         << " and atol = " << atol << endl;

    //   loop over output step sizes: call solver and output error
    while (tcur < 0.99999*Tf) {
      
      // set the time interval for this solve
      vector<double> tspan = {tcur, std::min(tcur + dtout, Tf)};

      // call the solver for this time interval
      vector<double> tvals = AE.Evolve(tspan, y);
      tcur = tvals.back();  // last entry in tvals
      totsteps += AE.steps;
      totfails += AE.fails;

      // compute the errors at tcur, output to screen, and accumulate maxima
      vector<double> yerr = y - ytrue(tcur);
      double abserr = InfNorm(yerr);
      double err = AE.rtol*InfNorm(ytrue(tcur)) + atol;
      maxabserr = std::max(maxabserr, abserr);
      maxerr = std::max(maxerr, err);
      cout << " (y1(" << tcur <<"), y2("<< tcur <<")) = (" << setprecision(6) << y[0] <<"," << y[1]<<")"
           << "\t abserr = " << abserr 
           << "\t err = " << err 
           << endl;
      
    }

    // output final results for this tolerance
    cout << "\nOverall results for rtol = " << AE.rtol << ":\n"
	 << "   maxabserr = " << maxabserr << endl
	 << "   maxerr = " << maxerr << endl
	 << "   steps = " << totsteps << endl
	 << "   fails = " << totfails << endl;

  }

  return 0;
}
