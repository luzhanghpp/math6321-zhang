 /* Generalize Trapezoidal time stepper class header file.

   Lu Zhang
   Math 6321 @ SMU
   Fall 2016  */

#ifndef GENERALTRAP_DEFINED__
#define GENERALTRAP_DEFINED__

// Inclusions
#include <vector>
#include <math.h>
#include "matrix.hpp"
#include "rhs.hpp"
#include "resid.hpp"
#include "newton.hpp"


// Backward Euler residual function class -- implements a 
// backward-Euler-specific ResidualFunction to be supplied 
// to the Newton solver.
class GeneTrapResid: public ResidualFunction {
public:

  // data required to evaluate backward Euler nonlinear residual
  RHSFunction *frhs;            // pointer to ODE RHS function
  double t;                     // current time
  double h;                     // current step size
  double theta;                 // weight in generalized trapezoid method
  std::vector<double> *yold;    // pointer to solution at old time step
  std::vector<double>  fold;    // extra vector for residual evaluation

  // constructor (sets RHSFunction and old solution pointers)
  GeneTrapResid(RHSFunction& frhs_, double theta_, std::vector<double>& yold_) {
    frhs = &frhs_; yold = &yold_;  fold = yold_; theta = theta_;
  };

  // residual evaluation routine
  int Evaluate(std::vector<double>& y, std::vector<double>& resid) {

    // evaluate RHS function at new time (store in resid)
    int ierr = frhs->Evaluate(t+h, y, resid);
    if (ierr != 0) {
      std::cerr << "Error in ODE RHS function = " << ierr << "\n";
      return ierr;
    }

    // evaluate RHS function at old time
    ierr = frhs->Evaluate(t, (*yold), fold);
    if (ierr != 0) {
      std::cerr << "Error in ODE RHS function = " << ierr << "\n";
      return ierr;
    }

    // combine pieces to fill residual, y-yold-h/2*[f(t+h,y)+f(t,yold)]
    resid = y - (*yold) - (1-theta)*h*fold - theta*h*resid;

    // return success
    return 0;
  }
};



// Backward Euler residual Jacobian function class -- implements 
// a backward-Euler-specific ResidualJacobian to be supplied 
// to the Newton solver.
class GeneTrapResidJac: public ResidualJacobian {
public:

  // data required to evaluate backward Euler residual Jacobian
  RHSJacobian *Jrhs;   // ODE RHS Jacobian function pointer
  double t;            // current time
  double h;            // current step size
  double theta;        // weight in generalized trapezoid method

  // constructor (sets RHS Jacobian function pointer)
  GeneTrapResidJac(RHSJacobian &Jrhs_, double theta_) {
    Jrhs = &Jrhs_; theta = theta_;
  };

  // Residual Jacobian evaluation routine
  int Evaluate(std::vector<double>& y, Matrix& J) {

    // evaluate RHS function Jacobian, Jrhs (store in J)
    int ierr = Jrhs->Evaluate(t+h, y, J);
    if (ierr != 0) {
      std::cerr << "Error in ODE RHS Jacobian function = " << ierr << "\n";
      return ierr;
    }
    // combine pieces to fill residual Jacobian,  J = I - theta*h*Jrhs
    J *= (-theta*h);
    for (int i=0; i<J.Rows(); i++)
      J(i,i) += 1.0;

    // return success
    return 0;
  }
};



// General Trapezoidal time stepper class
class GeneralizedTrapezoid {

 private:

  // private reusable local data
  std::vector<double> yold;   // old solution vector
  GeneTrapResid *resid;           // trapezoidal Euler residual function pointer
  GeneTrapResidJac *residJac;     // trapezoidal residual Jacobian function pointer

 public:

  NewtonSolver *newt;   // Newton nonlinear solver pointer

  // constructor (constructs residual, Jacobian, and copies y for local data)
  GeneralizedTrapezoid(RHSFunction& frhs_, RHSJacobian& Jrhs_, double theta_, 
                     std::vector<double>& y_);

  // destructor (frees pointers to local objects)
  ~GeneralizedTrapezoid() {
    delete resid;
    delete residJac;
    delete newt;
  };

  // Evolve routine (evolves the solution via trapezoidal)
  std::vector<double> Evolve(std::vector<double>& tspan, double h, 
                             std::vector<double>& y);

};

#endif
