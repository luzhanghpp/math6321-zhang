/* Main routine to test a set of LMM on the scalar-valued ODE problem 
     y' = lambda*y + 1/(1+t^2) - lambda*tan^(-1)(t), t in [0,3],
     y(0) = 0.

   Lu Zhang
   Math 6321 @ SMU
   Fall 2016  */

#include <iostream>
#include <vector>
#include "matrix.hpp"
#include "rhs.hpp"
#include "trapezoidal.hpp"
#include "lmm.hpp"

using namespace std;


// Define classes to compute the ODE RHS function and its Jacobian

//    ODE RHS function class -- instantiates a RHSFunction
class MyRHS: public RHSFunction {
public:
  double lambda;                                                    // stores some local data
  int Evaluate(double t, vector<double>& y, vector<double>& f) {    // evaluates the RHS function, f(t,y)
    f[0] = lambda*y[0] + 1/(1+t*t) - lambda*atan(t);
    return 0;
  }
};

//    ODE RHS Jacobian function class -- instantiates a RHSJacobian
class MyJac: public RHSJacobian {
public:
  double lambda;                                            // stores some local data
  int Evaluate(double t, vector<double>& y, Matrix& J) {    // evaluates the RHS Jacobian, J(t,y)
    J = 0.0;
    J(0,0) = lambda;
    return 0;
  }
};


// Convenience function for analytical solution
vector<double> ytrue(const double t) { 
  vector<double> yt = {atan(t)};
  return yt;
};



// main routine
int main() {

  // time steps to try
  vector<double> h = {0.1, 0.01, 0.001, 0.0001};

  // lambda values to try
  vector<double> lambda = {-1e+1, -1e+2, -1e+3, -1e+4};

  // set problem information
  vector<double> y0 = {0.0};
  long int M = 1;
  double t0 = 0.0;
  double Tf = 3.0;
  double dtout = 0.3;

  // create array for error values
  vector<double> errors1(h.size());
  vector<double> errors2(h.size());
  
  // create ODE RHS and Jacobian objects, store lambda value for this test
  MyRHS rhs;
  MyJac Jac;
  TrapezoidalStepper TR(rhs,Jac,y0);
  
  ////////// Adams-Moulton-2 //////////
  cout << "\n AM-2:\n";
  
  // loop over lambda values
  for(int il=0; il<lambda.size(); il++){

    cout << endl;

    // set value of lambda in rhs and Jac
    rhs.lambda = lambda[il];
    Jac.lambda = lambda[il];
    
    // create LMM method, set solver parameters
    vector<double> AM2_a = {1.0,0.0}; // make sure a.size()=b.size()-1
    vector<double> AM2_b = {5.0/12.0, 2.0/3.0, -1.0/12.0};
    LMMStepper AM2(rhs, Jac, y0, AM2_a, AM2_b);
    TR.newt->SetTolerances(1.e-9,1.e-11);
    TR.newt->SetMaxit(20);
    AM2.newt->SetTolerances(1.e-9, 1.e-11);
    AM2.newt->SetMaxit(20);

    // loop over time step sizes
    for (int ih=0; ih<h.size(); ih++) {
    cout << "  lambda = " << lambda[il] << ", h = " << h[ih];  

    // set the initial time
    double tcur = t0;
    double tout = t0 + dtout;

    // reset maxerr
    double maxerr = 0.0;

    // AM2 requires two initial conditions
    Matrix y_AM2(M,2);
    
    // first is just y0(insert into 2nd column of y_AM2)
    y_AM2[1] = y0;
    
    // second comes from implicit Euler step (insert into 1st column of y_BDF2)
    vector<double> tspan = {tcur, tcur+h[ih]};
    
    y_AM2[0] = y0;
    vector<double> tvals = TR.Evolve(tspan, h[ih], y_AM2[0]);
    
    // update tcur to end of initial conditions
    tcur += h[ih];
     
    // loop over output step sizes: call solver and output error
    while (tcur < 0.99999*Tf) {

      //set the time interval for this solve
      tspan = {tcur, tout};
      
      // call the solver, update current time
      vector<double> tvals = AM2.Evolve(tspan, h[ih], y_AM2);
      tcur = tvals.back();   // last entry in tvals

      // compute the error at tcur, output to screen and accumulate maximum
      vector<double> yerr = y_AM2[0] - ytrue(tcur);   // computed solution is in 1st column of y_AM2
      double err = InfNorm(yerr);
      maxerr = std::max(maxerr, err);

      // update output time for next solve
      tout = std::min(tcur + dtout, Tf);
    }

    // output Maximum error
    cout << "\t    Max error = " << maxerr << endl;
    errors1[ih] = maxerr;
    }

    // output convergence rate estimates
    cout << "  Convergence rates:  " << endl;
    for (int ih=1; ih<h.size(); ih++)
      cout << "  " << (log(errors1[ih])-log(errors1[ih-1]))/(log(h[ih])-log(h[ih-1])) << endl;
  }
 


   ////////// BDF-2 //////////
   cout << "\n BDF-2:\n";

   // loop over lambda values
   for(int il=0; il<lambda.size(); il++){

     cout << endl;

     // set value of lambda of rhs and Jac
     rhs.lambda = lambda[il];
     Jac.lambda = lambda[il];

     // set solver parameters
     vector<double> BDF2_a = {18.0/11.0, -9.0/11.0, 2.0/11.0};
     vector<double> BDF2_b = {6.0/11.0, 0.0, 0.0,0.0};
     LMMStepper BDF2(rhs, Jac, y0, BDF2_a, BDF2_b);
     TR.newt->SetTolerances(1.e-9, 1.e-11);
     TR.newt->SetMaxit(20);
     BDF2.newt->SetTolerances(1.e-9, 1.e-11);
     BDF2.newt->SetMaxit(20);

     // loop over time step sizes
     for (int ih=0; ih<h.size(); ih++) {
     cout << "  lambda = " << lambda[il] << ", h = " << h[ih];

     // set the initial time, first output time
     double tcur = t0;
     double tout = t0 + dtout;

     // reset maxerr
     double maxerr = 0.0;
     
     // BDF-2 requires three initial conditions
     Matrix y_BDF2(M,3);
     
     // first is just y0 (insert into 3rd column of y_BDF2)
     y_BDF2[2] = y0;
     
     // second comes from trapezoidal step (insert into 2nd column of y_BDF2)
     vector<double> tspan = {tcur, tcur+h[ih]};
     y_BDF2[1] = y0;
     vector<double> tvals = TR.Evolve(tspan, h[ih], y_BDF2[1]);

     // third one comes from trapezoidal step (insert into 2nd column of y_BDF2)
     tspan = {tcur,tcur+2*h[ih]};
     y_BDF2[0] = y0;
     TR.Evolve(tspan,h[ih], y_BDF2[0]);
    
     // update tcur to end of initial conditions
     tcur += 2* h[ih];

     // loop over output step sizes: call solver and output error
     while (tcur < 0.99999*Tf) {

       // set the time interval for this solve
       tspan = {tcur, tout};

       // call the solver, update current time
       tvals = BDF2.Evolve(tspan, h[ih], y_BDF2);
       tcur = tvals.back();   // last entry in tvals

       // compute the error at tcur, output to screen and accumulate maximum
       vector<double> yerr = y_BDF2[0] - ytrue(tcur);   // computed solution is in 1st column of y_BDF2
       double err = InfNorm(yerr);
       maxerr = std::max(maxerr, err);

       // update output time for next solve
       tout = std::min(tcur + dtout, Tf);
     }
     cout << "\t    Max error = " << maxerr << endl;
     errors2[ih] = maxerr;
     }
     // output convergence rate estimates
     cout << "  Convergence rates:  " << endl;
     for (int ih=1; ih<h.size(); ih++)
       cout << "  " << (log(errors2[ih])-log(errors2[ih-1]))/(log(h[ih])-log(h[ih-1])) << endl;
   }
   return 0;
}




  
