/* Extrapolate Multirate method time stepper class header file.

   Lu Zhang
   Math 6321 @ SMU
   Fall 2016  */

#ifndef Extrap_MULTIRATE_DEFINED__
#define Extrap_MULTIRATE_DEFINED__

// Inclusions
#include <vector>
#include <math.h>
#include "matrix.hpp"
#include "rhs.hpp"


// Extrapolate Multirate method time stepper class
class ExtrapMulti{

 private:

  // private reusable local data
  std::vector<double> f;   // storage for ODE RHS vector
  RHSFunction *frhsf;      // pointer to ODE RHS function (fast component)
  RHSFunction *frhss;      // pointer to ODE RHS function (slow component)

 public:

  // constructor (sets RHS function pointer, copies y for local data)
  ExtrapMulti(RHSFunction& frhsf_,RHSFunction& frhss_,  std::vector<double>& y) {
    frhsf = &frhsf_;
    frhss = &frhss_;
    f = y;
  };

  // Evolve routine (evolves the solution via forward Euler)
  std::vector<double> Evolve(std::vector<double>& tspan, double h, std::vector<double>& y, double m, double s);

  // get our extrapolate tabula
  Matrix Extrapolate(double s, double m, std::vector<double>& tspan, double h, std::vector<double> &y);
    
};

#endif
