/* test the first test problem for our multirate method

     y' = -2*(-1+y^2-cos(t))/2/y + 0.05*(-2+z^2-cos(5*t))/2/z - sin(t)/2/y;
     z' = 0.05*(-1+y^2-cos(t))/2/y-(-2+z*z-cos(5*t))/2/z-5*sin(5*t)/2/z;

     Lu Zhang
     Math 6321 @ SMU
     Fall 2016   */


#include <iostream>
#include <iomanip>
#include <vector>
#include "matrix.hpp"
#include "rhs.hpp"
#include "multi_rk2.hpp"
#include "partion_multi_rk2.hpp"
#include "extrap_multirate.hpp"


using namespace std;


// ODE RHS function class -- instantiates a RHSFunction(preparation for multirate RK2 method)
// for the whole ode system
class MyRHS: public RHSFunction {
public:
  int Evaluate(double t, vector<double>& y, vector<double>& f) {
      f[0] = -2.0*(-1.0+y[0]*y[0]-cos(t))/2.0/y[0]+0.05*(-2.0+y[1]*y[1]-cos(5.0*t))/2.0/y[1]-sin(t)/2.0/y[0];
      f[1] = 0.05*(-1.0+y[0]*y[0]-cos(t))/2.0/y[0]-(-2.0+y[1]*y[1]-cos(5.0*t))/2.0/y[1]-5.0*sin(5.0*t)/2.0/y[1];
    return 0;
  }
};

// ODE RHS function class -- instantiates a RHSFunction
// for the slow component
class MyRHSs: public RHSFunction {
public:
  int Evaluate(double t, vector<double>& y, vector<double>& f){
     f[0] = -2.0*(-1.0+y[0]*y[0]-cos(t))/2.0/y[0]+0.05*(-2.0+y[1]*y[1]-cos(5.0*t))/2.0/y[1]-sin(t)/2.0/y[0];
    return 0;
  }
};

// ODE RHS function class -- instantiates a RHSFunction
// for the fast component
class MyRHSf: public RHSFunction {
public:
  int Evaluate(double t, vector<double>& y, vector<double>& f){
      f[1] = 0.05*(-1.0+y[0]*y[0]-cos(t))/2.0/y[0]-(-2.0+y[1]*y[1]-cos(5.0*t))/2.0/y[1]-5.0*sin(5.0*t)/2.0/y[1];
    return 0;
  }
};

// Convenience function for analytical solution
vector<double> ytrue(const double t) { 
  vector<double> yt(2);
  yt[0] = sqrt(1.0+cos(t));
  yt[1] = sqrt(2.0+cos(5.0*t));
  return yt;
};

// main routine
int main() {

  // h to try
  vector<double> h = {0.2,0.1,0.02,0.01,0.002,0.001};

  // initialize our problem
  vector<double> y0 = {sqrt(2.0),sqrt(3.0)};
  double t0 = 0.0;
  double Tf = 0.4;
  double tcur = t0;
  double dtout = 0.2;
  double m = 5.0;
  double s = 3.0;   // after extrapolating, we can get (s-1)th order at most
  vector<double> errors(h.size());
  Matrix tra_errors(s,h.size());
  

  // create ODE RHS function objects
  MyRHS frhs;
  MyRHSs frhss;
  MyRHSf frhsf;

  // create Multirate RK2, Partitioned Multirate RK2 and Extrapolated Multirate stepper object
  MultiRK2 MRK2(frhs, frhsf, y0);
  PartitionedMultiRK2 PRK2(frhss, frhsf, y0);
  ExtrapMulti EM(frhsf,frhss,y0);

  // output our test problem 1
  cout << endl;
  cout << "The first test problem:" << endl;
  cout << "y' = -2*(-1+y^2-cos(t))/2/y + 0.05*(-2+z^2-cos(5*t))/2/z - sin(t)/2/y," << endl;
  cout << "z' = 0.05*(-1+y^2-cos(t))/2/y-(-2+z*z-cos(5*t))/2/z-5*sin(5*t)/2/z," << endl;
  cout << "y(0) = sqrt(2) and z(0) = sqrt(3)." << endl;
  
  //-----------------Multirate Runge Kutta 2 Method-----------------------
  cout << "\nRunning with Multirate RK2:" << endl;
  
  // loop over h
  for (int ih=0; ih<h.size(); ih++) {

    // initialize our problem
    vector<double> y(y0);
    tcur = t0;
    double maxerr = 0.0;
 
    // loop over output step sizes: call solver and output error
    while (tcur < 0.99999*Tf) {
      
      // set the time interval for this solve
      vector<double> tspan = {tcur, std::min(tcur + dtout, Tf)};

      // call the solver for this time interval
      vector<double> tvals = MRK2.Evolve(tspan, h[ih], m, y);
      tcur = tvals.back();  // last entry in tvals

      // compute the errors at tcur, and accumulate maxabserr
      vector<double> yerr = y - ytrue(tcur);
      double err = InfNorm(yerr);
      maxerr = std::max(maxerr, err);
    }

    // store the maximum error in our error vector
    errors[ih] = maxerr;

    //output results
    if (ih>0){
      cout << " h = " << h[ih] << "\t max err = " << maxerr << "\t conv rate ="
           << (log(errors[ih]) - log(errors[ih-1]))/(log(h[ih]) - log(h[ih-1])) << endl;
    }
      else{			      
	cout<< " h = " << h[ih] << "\t max err = " << maxerr << endl;
      }
    }

//--------------------Multirate Partitioned Runge Kutta 2 Method----------------------
  cout << "\nRunning with Multirate Partitioned RK2 Method:" << endl;
 
  // loop over h
  for (int ih=0; ih<h.size(); ih++) {

    // initialize our problem
    vector<double> y(y0);
    tcur = t0;
    double maxerr = 0.0;
 
    // loop over output step sizes: call solver and output error
    while (tcur < 0.99999*Tf) {
      
      // set the time interval for this solve
      vector<double> tspan = {tcur, std::min(tcur + dtout, Tf)};

      // call the solver for this time interval
      vector<double> tvals = PRK2.Evolve(tspan, h[ih], y);
      tcur = tvals.back();  // last entry in tvals

      // compute the errors at tcur, and accumulate maxabserr
      vector<double> yerr = y - ytrue(tcur);
      double err = InfNorm(yerr);
      maxerr = std::max(maxerr, err);
    }

    // store the maximum error in our error vector
    errors[ih] = maxerr;

    // output results
    if (ih>0){
      cout << " h = " << h[ih] << "\t max err = " << maxerr << "\t conv rate ="
           << (log(errors[ih]) - log(errors[ih-1]))/(log(h[ih]) - log(h[ih-1])) << endl;
    }
      else{			      
	cout<< " h = " << h[ih] << "\t max err = " << maxerr << endl;
      }
    }

  //---------------------Extrapolated Multirate Method-----------------------
  cout << "\nRunning with Extrapolate Multirate Method:" << endl;

  // calculate our true solution
  vector<double> yerr(2);
  vector<double> yt(2);
  yt = ytrue(Tf);
  
  // loop over h
  for(int ih=0;ih<h.size();ih++){

    // set our input value
    vector<double> y(y0);
  
    // set the time interval for this solve
    vector<double> tspan = {t0, Tf};

    // call extrapolate function to get more accuracy solution
    Matrix T = EM.Extrapolate(s,m,tspan,h[ih],y);

    // compute the errors at Tf
    for(double i=0.0;i<s;i++){
      yerr[0] = T(i,i)-yt[0];
      yerr[1] = T(i+s,i) - yt[1];
      tra_errors(i,ih) = InfNorm(yerr);
    }
 
  if (ih>0){
    cout << " h = " << h[ih] <<" \t max err = " << tra_errors(s-1,ih) << "\t conv rate =" 
	 << (log(tra_errors(0,ih)) - log(tra_errors(0,ih-1)))/(log(h[ih])-log(h[ih-1]))
	 << "\t  " << (log(tra_errors(1,ih)) - log(tra_errors(1,ih-1)))/(log(h[ih])-log(h[ih-1]))
	 << "\t  " << (log(tra_errors(2,ih)) - log(tra_errors(2,ih-1)))/(log(h[ih])-log(h[ih-1]))
      // << "\t  " << (log(tra_errors(3,ih)) - log(tra_errors(3,ih-1)))/(log(h[ih])-log(h[ih-1]))
      // << "\t  " << (log(tra_errors(4,ih)) - log(tra_errors(4,ih-1)))/(log(h[ih])-log(h[ih-1]))
         << endl;
  }
  else{
    cout << " h = " << h[ih] << " \t max err = " << tra_errors(s-1,ih)  << endl;
  }
  }
  return 0;
}

