/* Multirate time stepper class implementation file.
   We use Explicit Forward Euler as our base integration method

   Class to perform time evolution of the IVP
        y1' = f(t,y1,y2), 
        y2' = g(t,y1,y2),  
        y1(t0) = y10, y2(t0) = y20, t in [t0, Tf].
   where y1 is slow component and y2 is fast component.

   Lu Zhang
   Math 6321 @ SMU
   Fall 2016 */

#include <vector>
#include "matrix.hpp"
#include "extrap_multirate.hpp"


// The extrapolation multirate time step evolution routine
//
// Inputs:  tspan holds the current time interval, [t0, tf]
//          h holds the desired time step size
//          m holds the multiple between large step and small step, i.e, large step:h, small step: h/m
//          s holds the number of extrapolation
//          y holds the initial condition, y(t0)
// Outputs: y holds the computed solution, y(tf)

std::vector<double> ExtrapMulti::Evolve(std::vector<double>& tspan, double h, std::vector<double>& y, double m, double s) {

  // initialize vector which store times
  std::vector<double> times = {tspan[0]};

  // check for legal inputs 
  if (h <= 0.0) {
    std::cerr << "Extrap_Multi: Illegal h\n";
    return times;
  }
  if (tspan[1] <= tspan[0]) {
    std::cerr << "Extrap_Multi: Illegal tspan\n";
    return times;	  
  }

  // get the stepsize in our extrapolation for slow component
  h = h/s;
  
  // figure out how many time steps
  long int N = (tspan[1]-tspan[0]) / h;
  if (tspan[1] > tspan[0]+N*h)  N++;
    
  // iterate over time steps
  for (long int i=0; i<N; i++) {

    // last step only: update h to stop directly at final time
    if (i == N-1) 
      h = tspan[1]-times[i];

    double y1 = y[0];
    // compute ODE RHS(Here is the slow component)
    if (frhss->Evaluate(times[i], y, f) != 0) {
      std::cerr << "Extrap_Multi: Error in ODE RHS function\n";
      return times;
    }

    // update the slow component value
    y[0] = y[0] + h*f[0];
    double y2 = y[0];

    // compute ODE RHS(Here is the fast component, stepsize is h/m)
    for(double j=0; j<m; j++){
      y[0] = (m-j)/m*y1 + j/m*y2;  // linear interpolation
      if (frhsf->Evaluate(times[i]+j/m*h, y, f) != 0){
	std::cerr << "Multirate: Error in ODE RHS function\n";
	return times;
      }
	// update the fast componet value in every step
	y[1] = y[1] + h/m*f[1];	
      }
    y[0] = y2;

    // update our times
    times.push_back(times[i] + h);
  }

  return times;
}


// get our extrapolate tabula
Matrix ExtrapMulti::Extrapolate(double s, double m, std::vector<double>& tspan, double h, std::vector<double> &y){

  // n is a vector [1 2 3 4 ... m]
  std::vector<double> n;
  for(int k=0; k<s; k++){
    n.push_back(k+1);
  }

  // construct our extrapolate tabula
  // row 1:s is for slow component and row s+1:2s is for fast component
  Matrix T(2*s,s);
  for(double j=0; j<s; j++){
      std::vector<double> ytemp = y;
      Evolve(tspan, h, ytemp, m, j+1);
      T(j,0) = ytemp[0];
      T(j+s,0) = ytemp[1]; 
    }
  for(int i=1; i<s; i++){
    for(int k=1; k<=i; k++){
      T(i,k) = T(i,k-1) + (T(i,k-1) - T(i-1,k-1))/((n[i]/n[i-k])-1);
      T(i+s,k) = T(i+s,k-1) + (T(i+s,k-1) - T(i+s-1,k-1))/((n[i]/n[i-k])-1);
    }
  }
  return T;
}
     

     
 
