/* Multirate Runge-Kutta2 stepper class header file.

   Lu ZHang
   Math 6321 @ SMU
   Fall 2016  */

#ifndef MultiRate_RUNGE_KUTTA2_DEFINED__
#define MultiRate_RUNGE_KUTTA2_DEFINED__

// Inclusions
#include <vector>
#include <math.h>
#include "matrix.hpp"
#include "rhs.hpp"


// Multirate Runge-Kutta-2 time stepper class
class MultiRK2 {

 private:

  // private reusable local data
  RHSFunction *frhsf;          // pointer to ODE RHS function(for the fast component)
  RHSFunction *frhs;           // pointer to ODE RHS function(for the whole ODE system)
  std::vector<double> z, f0, f1, zf, ff0, ff1;     // local vector storage
  
  Matrix A;                   // bucture tabula
  std::vector<double> b, c;

 public:
  
  double h;           // current time step size for slow component
  
  MultiRK2(RHSFunction& frhs_, RHSFunction& frhsf_, std::vector<double>& y);

  // Evolve routine
  std::vector<double> Evolve(std::vector<double>& tspan, double h, double m, std::vector<double>& y);

  // single step routine in RK2
  int Step(double t, double h, double m, std::vector<double>& y);

};

#endif
