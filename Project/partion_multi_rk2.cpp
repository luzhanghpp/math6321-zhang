/* Partitioned Multirate Runge-Kutta2 solver class implementation file.

   Class to perform time evolution of the IVP system
        ys' = f(t,ys,yf),  t in [t0, Tf],  ys(t0) = ys0
        yf' = g(t,ys,yf),  t in [t0, Tf],  yf(t0) = yf0 

   LU Zhang
   Math 6321 @ SMU
   Fall 2016  */

#include "matrix.hpp"
#include "partion_multi_rk2.hpp"



// Partitioned Multirate Runge-Kutta2 class constructor routine
//
// Inputs:  frhss_ holds the ODE RHSFunction object( for the slow component)
//          frhsf_ holds the ODE RHSFunction object( for the fast component)
PartitionedMultiRK2::PartitionedMultiRK2(RHSFunction& frhss_, RHSFunction& frhsf_, std::vector<double>& y) {
  
  frhss = &frhss_;    // set RHSFunction pointer(for the slow component)
  frhsf = &frhsf_;    // set RHSFunction pointer(for the fast component)
  
  z = y;            // clone value to local vectors
  f0 = y;
  f1 = y;
  f2 = y;
  f3 = y;
 
  Af = Matrix(4,4);      // bucture tabula for the fast component
  Af(1,0) = 1.0/2.0;
  Af(2,0) = 1.0/4.0;
  Af(3,0) = 1.0/4.0;
  Af(2,1) = 1.0/4.0;
  Af(3,1) = 1.0/4.0;
  Af(3,2) = 1.0/2.0;
  bf = {1.0/4.0, 1.0/4.0, 1.0/4.0, 1.0/4.0};
  cf = {0.0, 1.0/2.0, 1.0/2.0, 1.0};

  As = Matrix(4,4);    // bucture tabula for the slow component 
  As(1,0) = 1.0;
  As(3,2) = 1.0;
  bs = {1.0/4.0, 1.0/4.0, 1.0/4.0, 1.0/4.0};
  cs = {0.0, 1.0, 0.0, 1.0};
  
  h = 0.0;      // set time step size
};


// The Partitioned Multirate Runge_Kutta2 time step evolution routine
//
// Inputs:  tspan holds the current time interval [t0, tf]
//          h holds the desired timestep size
//          y holds the initial condition, y(t0), note here y contains ys and yf
//
// Outputs: y holds the computed solution, y(tf), note here y contains ys and yf
//
// The return value is a row vector containing all internal 
// times at which the solution was computed,
//               [t0, t1, ..., tN]
std::vector<double> PartitionedMultiRK2::Evolve(std::vector<double>& tspan, double h, std::vector<double>& y) {

  // initialize output
  std::vector<double> times = {tspan[0]};

  // check for legal inputs
  if (h <= 0.0){
    std::cerr << "PartitioedMultiRK2: Illegal h\n";
    return times;
  }
  if (tspan[1] <= tspan[0]){
    std::cerr << "PartitionedMultiRK2: Illegal tspan\n";
    return times;
  }

  // figure out how many time steps
  long int N = (tspan[1] - tspan[0])/h;
  if (tspan[1] > tspan[0]+N*h) N++;

  // iterate over time steps
  for (long int i=0; i<N; i++){

    // last step only: update h to stop directly at final time
    if (i == N-1)
      h = tspan[1]-times[i];

    // call the Step function to update our y value
    if (Step(times[i], h, y) != 0){
      std::cerr << " Evolve error in RHS function\n";
      return times;
    }
	
    // update our times
    times.push_back(times[i]+h);
  }

  // set output array as the subset of tvals that we actually used
  return times;
}


// Single step of partioned multirate Runge-Kutta2 method
//
// Inputs:  t holds the current time
//          h holds the current time step size
//          z, f0-f5 hold temporary vectors needed for the problem
//          y holds the current solution
// Outputs: y holds the updated solution, y(t+h)
//
// The return value is an integer indicating success/failure,
// with 0 indicating success, and nonzero failure.
int PartitionedMultiRK2::Step(double t, double h, std::vector<double>& y) {

  // stage 1: set stage and compute RHS
  // for the slow component
  z = y;
  if (frhss->Evaluate(t, z, f0) != 0) {
    std::cerr << "Step: Error in ODE RHS function\n";
    return 1;
  }
  // for the fast component
  if (frhsf->Evaluate(t, z, f0) != 0) {
    std::cerr << "Step: Error in ODE RHS function\n";
    return 1;
  }

  // stage 2: set stage and compute RHS
  z[0] = y[0] + h*As(1,0)*f0[0];
  z[1] = y[1] + h*Af(1,0)*f0[1];
  // for the slow component
  if (frhss->Evaluate(t+cs[1]*h, z, f1) != 0) {
    std::cerr << "Step: Error in ODE RHS function\n";
    return 1;
  }
  // for the fast component
  if (frhsf->Evaluate(t+cf[1]*h, z, f1) != 0) {
    std::cerr << "Step: Error in ODE RHS function\n";
    return 1;
  }

  // stage 3: set stage and compute RHS
  z[0] = y[0] + h*(As(2,0)*f0[0] + As(2,1)*f1[0]);
  z[1] = y[1] + h*(Af(2,0)*f0[1] + Af(2,1)*f1[1]);
  // for the slow component
  if (frhss->Evaluate(t+cs[2]*h, z, f2) != 0) {
    std::cerr << "Step: Error in ODE RHS function\n";
    return 1;
  }
  // for the fast comonent
  if (frhsf->Evaluate(t+cf[2]*h, z, f2) != 0) {
    std::cerr << "Step: Error in ODE RHS function\n";
    return 1;
  }

  // stage 4: set stage and compute RHS
  z[0] = y[0] + h*(As(3,0)*f0[0] + As(3,1)*f1[0] + As(3,2)*f2[0]);
  z[1] = y[1] + h*(Af(3,0)*f0[1] + Af(3,1)*f1[1] + Af(3,2)*f2[1]);
  // for the slow component
  if (frhss->Evaluate(t+cs[3]*h, z, f3) != 0) {
    std::cerr << "Step: Error in ODE RHS function\n";
    return 1;
  }
  // for the fast component
   if (frhsf->Evaluate(t+cf[3]*h, z, f3) != 0) {
    std::cerr << "Step: Error in ODE RHS function\n";
    return 1;
  }

  // compute next step solution by 2nd order Runge kutta method
   y[0] += h*(bs[0]*f0[0] + bs[1]*f1[0] + bs[2]*f2[0] + bs[3]*f3[0]);
   y[1] += h*(bf[0]*f0[1] + bf[1]*f1[1] + bf[2]*f2[1] + bf[3]*f3[1]);

   // return success
   return 0;
}
