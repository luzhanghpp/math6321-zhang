/* Partitioned Multirate Runge-Kutta2 stepper class header file.

   Lu ZHang
   Math 6321 @ SMU
   Fall 2016  */

#ifndef Partitioned_MultiRate_RUNGE_KUTTA2_DEFINED__
#define Partitioned_MultiRate_RUNGE_KUTTA2_DEFINED__

// Inclusions
#include <vector>
#include <math.h>
#include "matrix.hpp"
#include "rhs.hpp"


// Partitioned Multirate Runge-Kutta-2 time stepper class
class PartitionedMultiRK2 {

 private:

  // private reusable local data
  RHSFunction *frhsf;          // pointer to ODE RHS function(for the fast component)
  RHSFunction *frhss;          // pointer to ODE RHS function(for the slow component)
  std::vector<double> z, f0, f1, f2, f3;     // local vector storage
  
  Matrix Af;                  // butcher tabula for the fast component
  std::vector<double> bf, cf;
  
  Matrix As;                  // butcher tabula for the slow component
  std::vector<double> bs, cs;

 public:
  
  double h;           // current time step size
  
  PartitionedMultiRK2(RHSFunction& frhss_, RHSFunction& frhsf_, std::vector<double>& y);

  // Evolve routine
  std::vector<double> Evolve(std::vector<double>& tspan, double h, std::vector<double>& y);

  // Single step function in RK2
  int Step(double t, double h, std::vector<double>& y);

};

#endif
