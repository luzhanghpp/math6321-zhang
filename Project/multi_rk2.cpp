/* Multirate Runge-Kutta2 solver class implementation file.

   Class to perform time evolution of the IVP system
        ys' = f(t,ys,yf),  t in [t0, Tf],  ys(t0) = ys0
        yf' = g(t,ys,yf),  t in [t0, Tf],  yf(t0) = yf0 

   LU Zhang
   Math 6321 @ SMU
   Fall 2016  */

#include "matrix.hpp"
#include "multi_rk2.hpp"



// Multirate Runge-Kutta2 class constructor routine
//
// Inputs:  frhs_ holds the ODE RHSFunction object.( for the whole ODE system)
//          frhsf_ holds the ODE RHSFunction object, g(t,ys,gy).( for fast component)
MultiRK2::MultiRK2(RHSFunction& frhs_, RHSFunction& frhsf_, std::vector<double>& y) {
  
  frhs = &frhs_;    // set RHSFunction pointer
  frhsf = &frhsf_;
  
  z = y;            // clone value to local vectors for the whole ode system
  f0 = y;
  f1 = y;
  
  zf = y;          // clone value to local vectors for the fast component
  ff0 = y;
  ff1 = y;
 
  A = Matrix(2,2);      // bucture tabula
  A(1,0) = 1.0;
  b = {1.0/2.0, 1.0/2.0};
  c = {0.0, 1.0};
  
  h = 0.0;      // set time step size for the slow component
};


// The Multirate Runge_Kutta2 time step evolution routine
//
// Inputs:  tspan holds the current time interval [t0, tf]
//          h holds the desired stepsize for the slow component
//          m holds the multiple between slow component's stepsize and fast component's stepsize
//          y holds the initial condition, y(t0), note here y contains ys and yf
//
// Outputs: y holds the computed solution, y(tf), note here y contains ys and yf
//
// The return value is a row vector containing all internal 
// times at which the solution was computed,
//               [t0, t1, ..., tN]
std::vector<double> MultiRK2::Evolve(std::vector<double>& tspan, double h, double m, std::vector<double>& y) {

  // initialize output
  std::vector<double> times = {tspan[0]};

  // check for legal inputs
  if (h <= 0.0){
    std::cerr << "MultiRK2: Illegal h\n";
    return times;
  }
  if (tspan[1] <= tspan[0]){
    std::cerr << "MultiRK2: Illegal tspan\n";
    return times;
  }

  // figure out how many time steps
  long int N = (tspan[1] - tspan[0])/h;
  if (tspan[1] > tspan[0]+N*h) N++;

  // iterate over time steps
  for (long int i=0; i<N; i++){

    // last step only: update h to stop directly at final time
    if (i == N-1)
      h = tspan[1]-times[i];

    // call Step function to envolve our y value at final time
    if (Step(times[i], h, m, y) != 0){
      std::cerr << " Evolve error in RHS function\n";
      return times;
    }
	
    // update our times
    times.push_back(times[i]+h);
  }

  // set output array as the subset of tvals that we actually used
  return times;
}


// Single step of multirate Runge-Kutta2 method
//
// Inputs:  t holds the current time
//          h holds the current time step size
//          z, f0, f1, zf, ff0, ff1 hold temporary vectors needed for the problem
//          y holds the current solution
// Outputs: y holds the updated solution, y(t+h)
//
// The return value is an integer indicating success/failure,
// with 0 indicating success, and nonzero failure.
int MultiRK2::Step(double t, double h, double m, std::vector<double>& y) {
  
  // copy initial slow component value to ytemp1
  double ytemp1 = y[0];

  // for slow component
  // stage 1
  z = y;
  if (frhs-> Evaluate(t,z,f0) != 0){
    std::cerr << " Error in RHSfunction\n";
    return 1;
  }

  // stage 2
  z = y + h*A(1,0)*f0;
  if(frhs-> Evaluate(t+c[1]*h,z,f1) != 0){
    std::cerr << " Error in RHSfunction\n";
    return 1;
  }

  // update our slow component value
  y[0] += h*(b[0]*f0[0] + b[1]*f1[0]);

  // copy updated y[0] to ytemp2
  double ytemp2 = y[0];

  // for fast component
  // repeate RK2 on fast component over the interval of slow componet
  for(double i=0;i<m;i++){

    // stage 1
    // linear interpolation to get the stage value on slow component
    zf[0] = (m-i)/m*ytemp1 + i/m*ytemp2;
    zf[1] = y[1];
    if( frhsf-> Evaluate(t+i/m*h,zf,ff0) != 0){
      std::cerr << " Error in RHSfunction\n";
      return 1;
    }

    // stage 2
    // linear interpolation to get the stage value on slow component
    zf[0] = (m-i-1.0)/m*ytemp1 + (i+1.0)/m*ytemp2;
    zf[1] = y[1] + h/m*A(1,0)*ff0[1];
    if( frhsf-> Evaluate(t+i/m*h+c[1]*h/m,zf,ff1) !=0){
      std::cerr << " Error in RHSfunction\n";
      return 1;
    }

    // update our fast componet value
    y[1] += h/m*(b[0]*ff0[1] + b[1]*ff1[1]);
  }

  // reset our updated slow component
  y[0] = ytemp2;

  // return success
  return 0;
}
