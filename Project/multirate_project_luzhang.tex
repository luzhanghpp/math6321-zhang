

\documentclass{beamer}
\usepackage{beamerthemeshadow}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
 \usepackage{multirow}
\usepackage[T1]{fontenc}
\setbeamertemplate{caption}{\insertcaption}
\usepackage{color}


%\usetheme{Amsterdam}
\usetheme{Berkeley}
\newtheorem{thm}{Theorem}[section]
\newtheorem{lem}{Lemma}[section]
\newtheorem{prop}{Proposition}[section]
\newtheorem{defn}{Definition}[section]
\newtheorem{ex}{Example}[section]
\newtheorem{cor}{Corollary}[section]
\newtheorem{rem}{Remark}[section]
\newtheorem{rems}{Remarks}[section]

\begin{document}

\title{Three Integration Methods for Multirate ODE System }
\institute{Department of Computational Mathematics \\Southern Methodist University\\Dallas, Texas\\
}
\author{Lu Zhang}
\date{Dec. 4, 2016}

\frame{\titlepage}

\section{Introduction}
\frame{\frametitle{Introduction}

\begin{itemize}
\item Multirate Method is used to solve the ODE system where some components evolve on a much faster/slower time scale than others.
\item Multirate methods use different time steps for different components.
\item Multirate methods is much more efficient than single step-size method for this kind of system.
\item An ordinary integration scheme is limited by the faster changing component.
\end{itemize}

}

\frame{\frametitle{Introduction}
Currently, We have several main Multirate Methods:\\

\begin{itemize}
\item Multi-step Method: use multi-step methods, partitions are fixed, which means our step sizes for different components are different but fixed.
\item Multi-stage method: Partitioned Runge-Kutta Method. We use different Butcher tables for different components, and our time step sizes are still fixed.
\item Self adjusting strategy: There is a mechanism to choose the time step size automatically. We don't need to fix the time-step sizes for the different components of the ODE system.
\end{itemize}

}

\frame{\frametitle{Introduction}
\begin{itemize}
\item Overall order of Multirate Method:\\
\indent \item The overall order of the multirate method with different integators depends on the order of integrators, the interpolation method and the order of their 'coupling'.\\
\indent \item For example, When the base method(our integrator) is order $p$ and the interpolation is order $p$ or $p-1$, then we can get the overall order for our multirate method is $p$.
\end{itemize}
}
 
\frame{\frametitle{Introduction}
Here, we will focus on the following three Multirate Methods:\\

\begin{itemize}
\item Second Order Explicit Runge-Kutta Multirate Method.
\item Multirate Partitioned Runge-Kutta Method(MPRK-2).
\item Extraplate Forward-Euler Multirate Method.
\end{itemize}

}

\section{First Method}
\frame{\frametitle{Second Order Explicit Runge-Kutta Multirate Method}
Suppose we have our IVP problem as follows:\\
\begin{equation}\label{1}
\left\{
\begin{array}{ll}
y' = f(t,y), &t \in (t_{0},Tf),     \\
y(t_{0}) = y_0.
\end{array}
\right.
\end{equation}

\begin{itemize}
\item $f(t,y)$ has a fast portion $f_{f}(t,y_{s},y_{f})$ and a slow portion $f_{s}(t,y_{s},y_{f})$, where
\item $y_{s}$ is the slow portion of $y$
\item $y_{f}$ is the fast portion of $y$
\end{itemize}

}

\frame{\frametitle{Second Order Explicit Runge-Kutta Multirate Method}
Based on slow and fast components, we partition problem $(1)$ as follows : \\
\begin{equation}\label{2}
\left\{
\begin{array}{ll}
y^{'}_{s}(t) = f_{s}(t,y_{s}(t),y_{f}(t)), &t \in (t_{0},Tf),     \\
y^{'}_{f}(t) = f_{f}(t,y_{s}(t),y_{f}(t)), &t \in (t_{0},Tf),     \\
y_{s}(t_{0}) = y_{s,0},      \\
y_{f}(t_{0}) = y_{f,0}.
\end{array}
\right.
\end{equation}

\begin{itemize}
\item Slow time-step size $h$ for slow component $y^{'}_{s} = f_{s}(t,y_{s}(t),y_{f}(t))$  \\
\item Fast time-step size $\frac{h}{m}$ for fast component $y^{'}_{f} = f_{f}(t,y_{s}(t),y_{f}(t))$
\end{itemize}

}

\frame{\frametitle{Second Order Explicit Runge-Kutta Multirate Method}
Butcher Table for the Explict RK2 Method:\\
\begin{tabular}{c|cc}
0& 0 &~\\
1& 1 &0\\
\hline
&0.5 &0.5
\end{tabular}\\
\vspace{6pt}
\vspace{6pt}
Recall our $s$ stage explicit Runge-Kutta method compute the next step solution $y_{n+1}$ from the current solution $y_n$ and $t_n$
using the formula:\\
\begin{equation*}
\begin{aligned}
&k_{i} = h*f(t+c_ih,y_n+h\sum_{j=1}^{i-1}a_{ij}k_j), \\
&y_{n+1} = y_n+\sum_{i=1}^{s}b_ik_i.
\end{aligned}
\end{equation*}
}

\frame{\frametitle{Second Order Explicit Runge-Kutta Multirate Method}
Now, use slow time-step size $h$ to get $y_{s}(t_{n+1})$ from $y_{s}(t_{n})$ and $y_{f}(t_{n})$.\\
\begin{equation*}
\begin{aligned}
k_{1} = & h*f(t_{n}, y_{s,n}, y_{f,n}), \\
k_{2} = & h*f(t_{n} + c_{2}*h, y_{s,n} + a_{2,1}*k_{1}[0],\\
&y_{f,n} + a_{2,1}*k_{1}[1]),\\
y_{s,n+1} = & y_{s,n} + b_{1}*k_{1}[0] + b_{2}*k_{2}[0].
\end{aligned}
\end{equation*}

}

\frame{\frametitle{Second Order Explicit Runge-Kutta Multirate Method}
\begin{itemize}
\item Use fast time-step size $\frac{h}{m}$ to get $y_{f}(t_{n+\frac{1}{m}})$, $y_{f}(t_{n+\frac{2}{m}})$ until $y_{f}(t_{n+1})$.\\
\item Use the first order interpolation to get the intermidate value of the slow component $y_{s}(t_{n+\frac{i}{m}})$, $i = 1,2...m-1$.\\
\item Calculate fast component by $2^{nd}$ order ERK method.
\end{itemize}
\begin{equation*}
\begin{aligned}
&y_{s}(t_{n + \frac{i-1}{m}}) = \frac{m-i+1}{m}*y_{s}(t_{n})+\frac{i-1}{m}*y_{s}(t_{n+1}),\\
&y_{s}(t_{n + \frac{i}{m}}) = \frac{m-i}{m}*y_{s}(t_{n})+\frac{i}{m}*y_{s}(t_{n+1}),\\
&k_{1}[1] = \frac{h}{m}*f_{f}(t_{n+\frac{i-1}{m}}, y_{s,n+\frac{i-1}{m}}, y_{f,n+\frac{i-1}{m}}), \\
&k_{2}[1] = \frac{h}{m}*f_{f}(t_{n+\frac{i-1}{m}}+c_{2}*\frac{h}{m}, y_{s,n+\frac{i}{m}},y_{f,n+\frac{i-1}{m}}+a_{21}*k_{1}[1]),\\
&y_{f,n+\frac{i}{m}} = y_{f,n+\frac{i-1}{m}} + b_{1}*k_{1}[1] + b_{2}*k_{2}[1].
\end{aligned}
\end{equation*}

}

\section{Second Method}
\frame{\frametitle{Multirate Partitioned Runge-Kutta Method}
\begin{itemize}
\item Partitioned Runge-Kutta method use different Butcher tables for different components.\\
\item Partitioned Runge-Kutta method use the same time-step size for both slow and fast components, but different Butcher tables.\\
\item For the base method:\\
\begin{tabular}{c|c}
c &a\\
\hline
&b
\end{tabular}\\
where $c$ is a column vector, $b$ is a row vector and $a$ is a square matrix.
\end{itemize}
}

\frame{\frametitle{Multirate Partitioned Runge-Kutta Method}
\begin{itemize}
\item For the slow component \cite{A1}:\\
\begin{tabular}{c|cccccc}
c &a\\
c &~ &a\\
. &~ &~ &.\\
. &~ &~ &~ &.\\
. &~ &~ &~ &~ &.\\
c &~ &~ &~ &~ &~ &a\\
\hline
&b/m &b/m &. &. &. &b/m
\end{tabular}
\end{itemize}
}

\frame{\frametitle{Multirate Partitioned Runge-Kutta Method}
\begin{itemize}
\vspace{6pt}

\item For the fast component \cite{A1}:\\
\begin{tabular}{c|cccccc}
c/m &a/m\\
I/m+c/m &I*b/m &a/m\\
. &. &. &.\\
. &. &~ &. &.\\
. &. &~ &~ &. &.\\
((m-1)/m)I+c/m &I*b/m &. &. &. &I*b/m &a/m\\
\hline
&b/m &b/m &. &. &. &b/m
\end{tabular}\\
where $I$ is a column vector whose elements are $1$.
\end{itemize}

}

\frame{\frametitle{Multirate Partitioned Runge-Kutta Method}
Here we focus on the order two multirate partitioned Runge-Kutta method(MPRK-2) whose $m=2$ \cite{A1}.\\
\begin{itemize}
\item Base method:\\
\begin{tabular}{c|cc}
0 &0 &~\\
1 &1 &0\\
\hline
&0.5 &0.5
\end{tabular}
\vspace{6pt}

\item For the slow component:\\
\begin{tabular}{c|cccc}
0 &0 &~ &~ &~\\
1 &1 &0 &~ &~\\
0 &0 &0 &0 &~\\
1 &0 &0 &1 &0\\
\hline
&0.25 &0.25 &0.25 &0.25
\end{tabular}
\end{itemize}

}

\frame{\frametitle{Multirate Partitioned Runge-Kutta Method}
Here we focus on the Multirate 2nd partitioned Runge-Kutta Method.\\
\begin{itemize}
\item For the fast component:\\
\begin{tabular}{c|cccc}
0 &0 &~ &~ &~\\
0.5 &0.5 &0 &~ &~\\
0.5 &0.25 &0.25 &0 &~\\
1 &0.25 &0.25 &0.5 &0\\
\hline
&0.25 &0.25 &0.25 &0.25
\end{tabular}
\end{itemize}
}

\frame{\frametitle{Multirate Partitioned Runge-Kutta Method}
\begin{itemize}
\item Stage and solution updates:\\
\begin{equation*}
\begin{aligned}
ks_{1} = & h*f_{s}(t_{n}, y_{s,n}, y_{f,n}), \\
kf_{1} = & h*f_{f}(t_{n}, y_{s,n}, y_{f,n}), \\
ks_{2} = & h*f_{s}(t_{n} + cs_{2}*h, y_{s,n} + as_{2,1}*ks_{1},\\
&y_{f,n} + af_{2,1}*kf_{1}),\\
kf_{2} = & h*f_{f}(t_{n} + cf_{2}*h, y_{s,n} + as_{2,1}*ks_{1},\\
&y_{f,n} + af_{2,1}*kf_{1}),\\
ks_{3} = & h*f_{s}(t_{n}, y_{s,n}, y_{f,n} + af_{31}*kf{1} + af_{32}*kf{2}),\\
kf_{3} = & h*f_{f}(t_{n} + cf_{3}*h, y_{s,n}, y_{f,n} + af_{31}*kf_{1}\\
&+ af_{32}*kf_{2}).\\
\end{aligned}
\end{equation*}
\end{itemize}
}

\frame{\frametitle{Multirate Partitioned Runge-Kutta Method}
\begin{itemize}
\item Stages and solution update:
\begin{equation*}
\begin{aligned}
ks_{4} = & h*f_{s}(t_{n} + cs_{4}*h, y_{s,n} + h*as_{43}*ks_{3}, \\
& y_{f,n} + af_{41}*kf{1} + af_{42}*kf{2} + af_{43}*kf_{3}),\\
kf_{4} = & h*f_{f}(t_{n} + cf_{4}*h, y_{s,n} + as_{43}*ks_{3}, \\
& y_{f,n} + af_{41}*kf_{1} + af_{42}*kf_{2} + af_{43}*kf_{3}),\\
y_{s,n+1} = & y_{s,n} + bs_{1}*ks_{1} + bs_{2}*ks_{2} + bs_{3}*ks_{3} + bs_{4}*ks_{4},\\
y_{f,n+1} = & y_{f,n} + bf_{1}*kf_{1} + bf_{2}*kf_{2} + bf_{3}*kf_{3} + bf_{4}*kf_{4}.\\
\end{aligned}
\end{equation*}
\end{itemize}
}

\section{Third Method}
\frame{\frametitle{Extrapolate Multirate Forward Euler Method}
\begin{itemize}
\item Use different time-step sizes for different components\\
\item Use h as our slow component's time-step size\\
\item Use $\frac{h}{m}$ as our fast component's time-step size\\
\item Use Forward Euler to be our base method\\
\item Our m-rate multirate explicit euler method:
\begin{equation*}
\begin{aligned}
y_{s,n+1} = &y_{s,n} + h*f_{s}(t_{n},y_{s,n},y_{f,n}),\\
y_{s,n+\frac{i-1}{m}} = &\frac{m-i+1}{m}*y_{s,n} + \frac{i-1}{m}*y_{s,n+1}, \\
y_{f,n+\frac{i}{m}} = &y_{f,n+\frac{i-1}{m}} + \frac{h}{m}*f_{f}(t_{n+\frac{i-1}{m}},\\
&y_{s,n+\frac{i-1}{m}},y_{f,n+\frac{i-1}{m}}), i = 1,...,m\\
\end{aligned}
\end{equation*}
\end{itemize}
}

\frame{\frametitle{Extrapolate Multirate Forward Euler Method}
\begin{itemize}
\item We use Atiken-Neville formula to be our extrapolation method \cite{A2,A3,A4}:
\begin{equation*}
T_{j,k+1} = T_{j,k} + \frac{T_{j,k} - T_{j-1,k}}{(n_{j}/n_{j-1}) - 1},j\leq s, k<j,
\end{equation*}
where $n_{i} < n_{i+1}\in \mathcal{N}$, here we choose the harmonic sequence $n_{j} = 1,2,...$, this choice is the most economical \cite{A5}.\\
\vspace{6pt}
\item By using extrapolate method, we can get higher order accuracy but we will lose our efficient.
\end{itemize}
}

\section{Numerical Results}
\frame{\frametitle{Results of The First Test Problem}
\begin{itemize}
\item Integrate over $0\leq t \leq 0.3$
\item Initial condition $y_{s}(0) = \sqrt{2}$, $y_{f}(0) = \sqrt{3}$.
\item Our initial value problem:
\begin{equation*}
\begin{aligned}
y_{s}^{'} = & -2*\frac{-1+y_{s}^{2}-\cos(t)}{2*y_{s}} + 0.05*\frac{-2+y_{f}^{2}-\cos(5*t)}{2*y_{f}} \\
&- \frac{\sin(t)}{2*y_{s}},\\
y_{f}^{'} = & 0.05*\frac{-1+y_{s}^{2}-\cos(t)}{2*y_{s}} - \frac{-2+y_{f}^{2}-\cos(5*t)}{2*y_{f}} \\
&- 5*\frac{\sin(5*t)}{2*y_{f}}.
\end{aligned}
\end{equation*}
\end{itemize}
}

\frame{\frametitle{Results of The First Test Problem}
\begin{table}
\centering
\begin{tabular}{|c|c|c|c}
\hline
h &max err &conv rate\\
\hline
0.2 &0.00144873 &~\\
0.1 &0.000346835 &2.06247\\
0.02 &1.33939e-05 &2.02185\\
0.01 &3.33328e-06 &2.00656\\
0.002 &1.32843e-07 &2.00228\\
0.001 &3.31954e-08 &2.00066\\
\hline
\end{tabular}
\caption{Results of Multirate Second Order Runge Kutta Method}
\end{table}
}

\frame{\frametitle{Results of The First Test Problem}
\begin{table}
\centering
\begin{tabular}{|c|c|c|c}
\hline
h &max err &conv rate\\
\hline
0.2 &0.00648665 &~\\
0.1 &0.00150932 &2.10357\\
0.02 &5.68232e-05 &2.03765\\
0.01 &1.40946e-05 &2.01103\\
0.002 &5.60469e-07 &2.0038\\
0.001 &1.4001e-07 &2.0011\\
\hline
\end{tabular}
\caption{Results of Multirate Partitioned Runge Kutta(MPRK-2) Method}
\end{table}
}

\frame{\frametitle{Results of The First Test Problem}
\begin{table}[tbp]
\centering
\begin{tabular}{|c|c|c|c|c|c}
\hline
h &max err &conv rate &cov rate&con rate\\
\hline
0.2 &5.40697e-05 &~ &~ &~\\
0.1 &5.26091e-06 &1.01507 &2.23295 &3.36143\\
0.02 &3.53891e-08 &1.00504 &2.06961 &3.1077\\
0.01 &4.33501e-09 &1.00144 &2.019 &3.0292\\
0.002 &3.41238e-11 &1.00049 &2.00647 &3.01005\\
0.001 &4.26748e-12 &1.00014 &2.00187 &2.99932\\
\hline
\end{tabular}
\caption{Results of Extrapolate Multirate Explicit Forward Euler Method}
\end{table}
}


\frame{\frametitle{Results of The Second Test Problem}
\begin{itemize}
\item Integrate over $0\leq t \leq 5$
\item Initial condition $y_{s}(0) = 1$, $y_{f}(0) = \frac{1}{1250}$.
\item Our initial value problem:
\begin{equation*}
\begin{aligned}
y_{s}^{'} = & \frac{y_{s}}{2}\\
y_{f}^{'} = & y_{s}*\cos(25*t).
\end{aligned}
\end{equation*}
\end{itemize}
}

\frame{\frametitle{Results of The Second Test Problem}
\begin{table}[tbp]
\centering
\begin{tabular}{|c|c|c|c}
\hline
h &max err &conv rate\\
\hline
0.2 &0.0400628 &~\\
0.1 &0.0122176 &1.7133\\
0.02 &0.000503802 &1.9811\\
0.01 &0.000126425 &1.99457\\
0.002 &5.07223e-06 &1.99813\\
0.001 &1.26853e-06 &1.99946\\
\hline
\end{tabular}
\caption{Results of Multirate Second Order Runge Kutta Method}
\end{table}
}

\frame{\frametitle{Results of The Second Test Problem}
\begin{table}[tbp]
\centering
\begin{tabular}{|c|c|c|c}
\hline
h &max err &conv rate\\
\hline
0.2 &0.145823 &~\\
0.1 &0.0413266 &1.81907\\
0.02 &0.00161731 &2.01359\\
0.01 &0.000404175 &2.00054\\
0.002 &1.6168e-05 &1.99996\\
0.001 &4.04212e-06 &1.99996\\
\hline
\end{tabular}
\caption{Results of Multirate Partitioned Runge Kutta(MPRK-2) Method}
\end{table}
}

\frame{\frametitle{Results of The Second Test Problem}
\begin{table}[tbp]
\centering
\begin{tabular}{|c|c|c|c|c|c}
\hline
h &max err &conv rate &cov rate&con rate\\
\hline
0.2 &0.00327654 &~ &~ &~\\
0.1 &0.000454134 &0.914388 &1.87523 &2.85097\\
0.02 &3.95765e-06 &0.968989 &1.95525 &2.94683\\
0.01 &5.00123e-07 &0.990764 &1.98674 &2.98429\\
0.002 &4.02366e-09 &0.996801 &1.99541 &2.99649\\
0.001 &5.2969e-10 &0.999069 &1.99867 &2.92529\\
\hline
\end{tabular}
\caption{Results of Extrapolate Multirate Explicit Forward Euler Method}
\end{table}
}


\frame{\frametitle{Results of The Third Test Problem}
\begin{itemize}
\item Integrate over $0\leq t \leq 5$
\item Initial condition $y_{s}(0) = 1$, $y_{f}(0) = 0$.
\item Our initial value problem:
\begin{equation*}
\begin{aligned}
y_{s}^{'} = & -\sin(t),\\
y_{f}^{'} = & 25*\cos(25*t)*y_{s} - \sin(25*t)*\sin(t).
\end{aligned}
\end{equation*}
\end{itemize}
}

\frame{\frametitle{Results of The Third Test Problem}
\begin{table}[tbp]
\centering
\begin{tabular}{|c|c|c|c}
\hline
h &max err &conv rate\\
\hline
0.1 &0.00937715 &~\\
0.02 &0.000372616 &2.00411\\
0.01 &9.31366e-05 &2.00027\\
0.005 &2.32831e-05 &2.00007\\
0.002 &3.72524e-06 &2.00001\\
0.001 &9.31309e-07 &2\\
\hline
\end{tabular}
\caption{Results of Multirate Second Order Runge Kutta Method}
\end{table}
}

\frame{\frametitle{Results of The Third Test Problem}
\begin{table}[tbp]
\centering
\begin{tabular}{|c|c|c|c}
\hline
h &max err &conv rate\\
\hline
0.1 &0.0558954 &~\\
0.02 &0.00208035 &2.04478\\
0.01 &0.000516778 &2.00921\\
0.005 &0.00012881 &2.0043\\
0.002 &2.0574e-05 &2.00188\\
0.001 &5.1406e-06 &2.00082\\
\hline
\end{tabular}
\caption{Results of Multirate Partitioned Runge Kutta(MPRK-2) Method}
\end{table}
}

\frame{\frametitle{Results of The Third Test Problem}
\begin{table}[tbp]
\centering
\begin{tabular}{|c|c|c|c|c|c}
\hline
h &max err &conv rate &cov rate&con rate\\
\hline
0.1 &0.000113645 &~ &~ &~\\
0.02 &1.02248e-06 &1.02778 &1.96065 &2.92701\\
0.01 &1.29527e-07 &1.00867 &1.98804 &2.98074\\
0.005 &1.62997e-08 &1.00439 &1.99398 &2.99034\\
0.002 &1.04945e-09 &1.00201 &1.99729 &2.99346\\
0.001 &1.21538e-10 &1.00089 &1.99879 &3.11016\\
\hline
\end{tabular}
\caption{Results of Extrapolate Multirate Explicit Forward Euler Method}
\end{table}
}

\section{Reference}
\frame[allowframebreaks]{\frametitle{Reference}
\begin{thebibliography}{}
\setbeamertemplate{bibliography item}[text]

\bibitem{A1} Emil M.Constantinescu Adrian Sandu
\newblock \emph{Multirate Timestepping Methods for Hyperbolic Conservation Laws},
\newblock Springer Science+Business Media,LLC,J Sci Comput \textbf{33} (2007), 239--278.

\bibitem{A2} A. Aitken
\newblock \emph{On interpolation by iteration of proportional parts wiyhout the use of differences},
\newblock Proc. Edinb. Math. Soc. \textbf{3}(1932), 56--76.

\bibitem{A3} M. Gasca, T. Sauer
\newblock \emph{Polynomial interpolation in several variables},
\newblock Adv. Comput. Math. \textbf{12}(2000), 377--410.

\bibitem{A4} E. Neville
\newblock \emph{Iterative interpolation},
\newblock J. Indian Math. Soc. \textbf{20}(1934) 87--120.

\bibitem{A5} Emil M. Constantinescu Adrian Sandu
\newblock \emph{Extrapolated Multirate Method for Differential Equations with Muliple Time Scales},
\newblock Springer Science+Business Media New York, J Sci Comput.\textbf{56}(2013), 28--44.

\end{thebibliography}
}


\frame{
\center{\emph{\Huge{Thank you!}}}
}



\end{document} 