/* Lu Zhang
   SMU Mathematics
   Fall 2016 */

// Inclusions
#include <iostream>
#include <cmath>
using namespace std;

int main()
{
  
  // declarations
  // x1 is the first initial solution guess
  // x2 is the second initial solution guess
  // x3 is the third initial solution guess
  // y1 is the polynomial for the first initial guess x1
  // y2 is the polynomial for the second initial guess x2
  // y3 is the polynomial for the third initial guess x3
  // dy1 is the derivative of the polynomial for the first initial guess
  // dy2 is the derivative of the polynomial for the second initial guess
  // dy3 is the derivative of the polynomial for the third initial guess
  // rtol is relative tolerance
  // atol is absolute tolerance
  double x1, x2, x3, y1, dy1, y2, dy2, y3, dy3;
  const double rtol = 1e-11;
  const double atol = 1e-10;

  // explain the aim of the program
  cout<<"This program is used to approximate the roots of the polynomial x^3 -3*x^2+3.0/2.0*x-1.0/6.0 by newton method."<<endl;
  cout<<"When you choose your initial guesses of solutions, you'd better draw a graph of the polynomial by other tools, and then choose your initial guesses."<<endl;
  cout<<"In order to get all of the roots of this polynomial, I suggest you choose x1 = 0.1, x2 = 0.4 and x3 = 3. But you can give your own initial guesses. But if you just give your inital guesses arbitrarily, you may need to run this program several times to get all of the solutions. "<<endl<<endl;
  
  // enter the first initial guess
  cout<<"please enter the first initial guess x1:"<<endl;

  // get the first initial guess and give it to x1
  cin>>x1;

  // enter the second initial guess
  cout<<"please enter the second initial guess x2:"<<endl;

  // get the second initial guess and give it to x2
  cin>>x2;

  // enter the third initial guess
  cout<<"please enter the third initial guess x3:"<<endl;
  // get the third initial guess and give it to x3
  cin>>x3;
  
  // begin our loops to get the first root of the polynomial
  for(int i=0;i<20;i++)
    {

      // expression of the polynomial and its derivative for the first initial guess
      y1 = x1*x1*x1 - 3*x1*x1 + 3.0/2.0*x1 - 1.0/6.0;
      dy1 = 3*x1*x1 - 2*3*x1 + 3.0/2.0;

      // update the first initial guess x1
      x1 = x1 - y1/dy1;

      // condtion of ending loop
      if(abs(y1/dy1)<atol+rtol*abs(x1))
	{
	  break;
	}
    }
 

// begin our loops to get the second root of the polynomial
  for(int j=0;j<20;j++)
    {

      // expression of the polynomial and its derivative for the second initial guess
      y2 = x2*x2*x2 - 3*x2*x2 + 3.0/2.0*x2 - 1.0/6.0;
      dy2 = 3*x2*x2 - 2*3*x2 + 3.0/2.0;

      // update the second initial guess x2
      x2 = x2 - y2/dy2;

      // condtion of ending loop
      if(abs(y2/dy2)<atol+rtol*abs(x2))
	{
	  break;
	}
    }


  // begin our loops to get the third root of the polynomial
  for(int k=0;k<20;k++)
    {

      // expression of the polynomial and its derivative for the third initial guess
      y3 = x3*x3*x3 - 3*x3*x3 + 3.0/2.0*x3 - 1.0/6.0;
      dy3 = 3*x3*x3 - 2*3*x3 + 3.0/2.0;

      // update the third initial guess x3
      x3 = x3 - y3/dy3;

      // condtion of ending loop
      if(abs(y3/dy3)<atol+rtol*abs(x3))
	{
	  break;
	}
    }
  
  // output the roots on screen
  cout<<endl<<"the roots of the polynomial are x1="<<x1<<", x2="<<x2<<" "<<"and x3="<<x3<<endl;
  return 0;
}


