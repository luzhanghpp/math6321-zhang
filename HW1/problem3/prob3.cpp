/* Lu Zhang
   SMU Mathematics
   Fall 2016 */

// Inclusions
#include <iostream>
#include <cmath>
#include <math.h>
#include "../../matrix/matrix.hpp"
using namespace std;

int main()
{

  // describe the program
  cout<<"This program is used to get a solution of the following nonlinear system by newton method."<<endl;
  cout<<"x^2 + y^2 = 4,"<<endl;
  cout<<"x*y = 1."<<endl<<endl;
  
  // define double a and b to get the input initial guess of x and y respextively
  double a,b;

  // define function matrix f(2,1), Jacobian matrix df(2,2) and arguement matrix X(2,1)
  Matrix f(2,1),df(2,2),X(2,1);

  // relative tolerance
  const double rtol=1e-6;

  // absolute tolerance
  const double atol=1e-10;
  
  // remind user to input initial guess
  cout<<"please enter the initial guess Xfirst=(x,y)=(1,2), you need to input two numbers 1 and 2 : "<<endl;

  // give initial guess to our arguement matrix
  cin>>a>>b;
  X(0) = a;
  X(1) = b;

  // begain our loops to get the final solution based on initial guess
  for(int i=0;i<20;i++)
    {

      // give value to our function matrix
      f(0) = X(0)*X(0) + X(1) * X(1) -4;
      f(1) = X(0)*X(1) -1;
      
      // give value to our Jacobi matrix
      df(0,0) = 2*X(0);
      df(0,1) = 2*X(1);
      df(1,0) = X(1);
      df(1,1) = X(0);

      
      // use linear solver to get difference of solutions
      Matrix tem_f = -1*f;
      Matrix h = LinearSolve(df,tem_f);

      // update solution X
      X.Copy(X+h);

      // condition to end our loops
      if(Norm(h)<rtol*Norm(X)+atol)
	{
	  break;
	}
    }

  // output our final solution X=(x,y)
  cout<<"the root is:x="<<X(0)<<"and y="<<X(1)<<endl;
  return 0;
}
